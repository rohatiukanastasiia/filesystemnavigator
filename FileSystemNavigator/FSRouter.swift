
import Foundation
import UIKit

protocol Router{
    func navigateToNext(element: Base)
    func prepare(for segue: UIStoryboardSegue, selectedElement: Base)
}

class FSRouting: Router{
    
    weak var viewController: UIViewController!
    init (viewController: UIViewController){
        self.viewController = viewController
    }
    
    func navigateToNext(element: Base){
        if let folder = element as? Folder {
            let nextViewController = viewController.storyboard?.instantiateViewController(withIdentifier: "ViewController") as! ViewController
            nextViewController.isRoot = false
            nextViewController.folder = folder
            viewController.navigationController?.pushViewController(nextViewController, animated: true)
        }else if element is File {
            viewController.performSegue(withIdentifier: "showfiledata", sender: viewController)
        }
    }
    
    func prepare(for segue: UIStoryboardSegue, selectedElement: Base) {
        if let vc = segue.destination as? FileViewer, let file = selectedElement as? File{
            vc.filecontet = file.content
        }
    }
}
