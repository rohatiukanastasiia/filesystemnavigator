
import Foundation

protocol Presenter{
    func presentFetchResults(response : FileSystemWorker.Response)
}

class FSPresenter: Presenter{
    
    var viewController: ViewControllerOutput!
    func presentFetchResults(response : FileSystemWorker.Response){
        if let message = response.message{
            viewController.handlerError(message: message)
        }else{
            guard let json = response.json,
                  let root = buildfile(data: json) as? Folder
             else {
                viewController.handlerError(message: FetchJSONError.invalidRootFolder)
                return
            }
            viewController.handlerSuccess(dataSource: root)
        }
    }
    
    private func buildfile(data: JSONfileData) -> Base {
        if data.isFolder{
            let folder = Folder(name: data.name)
            for subfile in data.content{
                folder.content.append(buildfile(data: subfile))
            }
            return folder
        }else{
            let file = File(name: data.name, icon: data.icon)
            if let fileContent = data.fileContent{
                file.content = fileContent
            }
            return file
        }
    }
}
