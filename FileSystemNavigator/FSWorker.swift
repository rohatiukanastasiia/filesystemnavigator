
import Foundation

typealias JSONResponse = (_ response: FileSystemWorker.Response) ->()
enum FetchJSONError: Error {
    case invalidFileName(String)
    case invalidRootFolder
}

class FileSystemWorker {
    
    struct Response
    {
        var json: JSONfileData?
        var message: Error?
    }
    
    func fetchJSON(filename: String, fileExtension: String, response:@escaping(JSONResponse)) {
        guard let fileurl = Bundle.main.url(forResource: filename, withExtension: fileExtension) else {
            response(Response(json: nil, message: FetchJSONError.invalidFileName("\(filename).\(fileExtension)")))
            return
        }
        do {
            let data = try Data(contentsOf: fileurl)
            if let json = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]{
                if let filedata = JSONfileData(json: json){
                    response(Response(json: filedata, message: nil))
                }
            }
        } catch {
            response(Response(json: nil, message: error))
        }
    }
}
