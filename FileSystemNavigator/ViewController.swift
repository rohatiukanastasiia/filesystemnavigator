
import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var filesystemcollection: UICollectionView!
    
    var router: Router?
    var interactor: Interactor?
    var folder: Folder? {
        willSet(newValue){
            self.title = newValue?.name
        }
    }
    var isRoot: Bool = true
    private var selected: Base?
    
    @available(iOS 6.0, *)
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return folder?.content.count ?? 0
    }

    @available(iOS 6.0, *)
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "foldercell", for: indexPath) as! FileSystemElementCell
        if let data = folder?.content[indexPath.row] {
            cell.nameLabel.text = data.name
            if (data.isFolder){
                cell.iconView.image = #imageLiteral(resourceName: "folder_icon")
            }else if let file = data as? File{
                cell.reloadIcon(icon: file.icon)
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selected = folder?.content[indexPath.row]
        if let selected = selected {router?.navigateToNext(element: selected)}
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let selected = selected {router?.prepare(for: segue,selectedElement: selected)}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout = filesystemcollection.collectionViewLayout as? UICollectionViewFlowLayout
        layout?.itemSize = CGSize(width: CGFloat((filesystemcollection.frame.size.width / 3) - 10), height: CGFloat(100))
        
        self.router = FSRouting(viewController: self)
        guard isRoot else {return}
        Configurator.sharedInstance.configure(viewController: self)
        interactor?.fetchfilesSystem(fileName: "fileSystem", fileExtension: "json")
    }
}

