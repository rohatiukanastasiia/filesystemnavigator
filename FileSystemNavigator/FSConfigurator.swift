
import Foundation
import UIKit

class Configurator {
    static let sharedInstance = Configurator()
    private init(){}
    
    func configure(viewController: ViewController)
    {
        let presenter = FSPresenter()
        presenter.viewController = viewController
        
        let interactor = FSInteractor()
        interactor.presenter = presenter
        
        viewController.interactor = interactor
    }
}

protocol ViewControllerOutput{
    func handlerSuccess(dataSource: Folder)
    func handlerError(message: Error)
}

extension ViewController: ViewControllerOutput {
    func handlerSuccess(dataSource: Folder) {
        folder = dataSource
    }
    
    func handlerError(message: Error) {
        var alert: UIAlertController
        switch message {
        case FetchJSONError.invalidRootFolder:
            alert = UIAlertController(title: "Error", message: "Invalid Root Folder", preferredStyle: .alert)
        case FetchJSONError.invalidFileName(let errorString):
            alert = UIAlertController(title: "Error", message: "Invalid file name \(errorString)", preferredStyle: .alert)
        default:
            alert = UIAlertController(title: "Error", message: message.localizedDescription, preferredStyle: .alert)
        }
        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .`default`, handler: { _ in
            NSLog("The \"OK\" alert occured.")
        }))
        self.present(alert, animated: true, completion: nil)
    }
}

extension FileSystemElementCell {
    func reloadIcon(icon: URL?) {
        if let url = icon{
            let session = URLSession(configuration: .default)
            let datatask: URLSessionDataTask = session.dataTask(with: url, completionHandler: {data,response,error in
                guard error == nil else {return}
                DispatchQueue.main.async {
                    if let image = data {
                        self.iconView.image = UIImage(data: image)
                    }
                }
            })
            datatask.resume()
        }
    }
}
