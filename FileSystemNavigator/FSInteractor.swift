
import Foundation

protocol Interactor{
    func fetchfilesSystem(fileName: String, fileExtension: String)
}

class FSInteractor: Interactor {
    
    var presenter: Presenter!
    func fetchfilesSystem(fileName: String, fileExtension: String) {
        let worker = FileSystemWorker()
        worker.fetchJSON(filename: fileName, fileExtension: fileExtension, response: presenter.presentFetchResults(response: ))
    }
}
