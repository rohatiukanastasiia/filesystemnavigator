
import Foundation

struct JSONfileData{
    enum FileAtr: String {
        case name, isFolder, icon, content
    }
    
    let name: String
    let isFolder: Bool
    var content: Array<JSONfileData> = []
    let icon: String?
    let fileContent: String?
    
    init?(json: [String: Any]){
        guard let name = json[FileAtr.name.rawValue] as? String, let isfolder = json[FileAtr.isFolder.rawValue] as? Bool else {return nil}
        self.name = name
        self.isFolder = isfolder
        
        self.icon = json[FileAtr.icon.rawValue] as? String
        self.fileContent = json[FileAtr.content.rawValue] as? String
        if let content = json[FileAtr.content.rawValue] as? [[String: Any]]{
            for contentfile in content {
                if let newfile = JSONfileData(json: contentfile) {
                    self.content.append(newfile)
                }
            }
        }
    }
}

class Base {
    var isFolder: Bool
    var name: String
    
    init(name: String, isFolder: Bool) {
        self.isFolder = isFolder
        self.name = name
    }
}

class File: Base {
    var content: String = ""
    var icon : URL?
    
    init(name: String, icon : String?){
        super.init(name: name, isFolder: false)
        if let iconpath = icon {self.icon = URL(string: iconpath)}
    }
}

class Folder: Base {
    var content: Array<Base> = []
    
    init(name: String){
        super.init(name: name, isFolder: true)
        
    }
}
